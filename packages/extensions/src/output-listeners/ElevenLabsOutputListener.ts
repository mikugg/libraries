
import axios from 'axios';
import * as Core from '@mikugg/core';

interface ElevenLabsOutputListenerConfig {
  apiUrl?: string;
  apiKey: string;
  voiceId: string;
}

export class ElevenLabsOutputListener extends Core.OutputListeners.OutputListener<Core.OutputListeners.DialogOutputEnvironment, ArrayBuffer> {
  private apiKey: string;
  private apiUrl: string;
  private voiceId: string;
  
  constructor({apiUrl, apiKey, voiceId}: ElevenLabsOutputListenerConfig) {
    super();
    this.apiUrl = apiUrl || 'https://api.elevenlabs.io/v1/text-to-speech';
    this.apiKey = apiKey;
    this.voiceId = voiceId;
  }

  private cleanText(text: string) {
    let cleanText = "";
    let lastOpen: undefined | string = undefined;
    for (let x = 0; x < text.length; x++)
    {
        const ch = text.charAt(x);

        if (lastOpen == '(' && ch == ')') {lastOpen = undefined; continue;}
        if (lastOpen == '[' && ch == ']') {lastOpen = undefined; continue;}
        if (lastOpen == '-' && ch == '-') {lastOpen = undefined; continue;}
        if (lastOpen == '*' && ch == '*') {lastOpen = undefined; continue;}

        if (ch == '(' || ch == '[' || ch == '-' || ch == "*") {
          lastOpen = ch;
          continue;
        }

        if (!lastOpen) {
          cleanText += ch;
        }

        
    }
    cleanText.replace(/ *\([^)]*\) */g, "");

    return cleanText;
  }

  protected async handleOutput(output: Core.OutputListeners.DialogOutputEnvironment): Promise<ArrayBuffer> {
    const text = this.cleanText(output.text);
    return this.synthesize(text);
  }

  private async synthesize(text: string): Promise<ArrayBuffer> {
    const requestUrl = `${this.apiUrl}/${this.voiceId}`;
    return axios.post<ArrayBuffer>(requestUrl, { text }, {
      headers: {
        'Accept': 'audio/mpeg',
        'xi-api-key': this.apiKey,
      },
      'responseType': "arraybuffer"
    }).then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.log("Error: ", err);
      return new ArrayBuffer(0);
    });
  }
}