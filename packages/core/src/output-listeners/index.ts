export const moods = ['happy', 'sad', 'neutral', 'angry', 'confused', 'surprised', 'disgusted', 'fearful', 'unchanged'] as const;
export type Mood = typeof moods[number];

export interface OutputEnvironment {
  text: string
}

export interface DialogOutputEnvironment extends OutputEnvironment {
  mood: Mood;
}

export interface ActionOutputEnvironment extends OutputEnvironment {
  mood: Mood;
}

export interface ContextOutputEnvironment extends OutputEnvironment {
  description: string;
  keywords: string;
}

export interface OptionsOutputEnvironment extends OutputEnvironment {
  options: string[];
}

export abstract class OutputListener<T extends OutputEnvironment, W = void>  {
  private subscriptions: Set<(result: W) => void> = new Set();

  protected abstract handleOutput(output: T): Promise<W>;

  public sendOutput(output: T): void {
    this.handleOutput(output).then((result: W) => this.subscriptions.forEach(fn => fn(result)));
  }

  public subscribe(fn: (output: W) => void): () => void {
    this.subscriptions.add(fn);
    return () => this.subscriptions.delete(fn);
  }
}

export class SimpleListener<T extends OutputEnvironment> extends OutputListener<T, T>  {
  constructor() {
    super();
  }

  protected async handleOutput(output: T): Promise<T> {
    return output;
  }
}
